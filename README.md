# OpenML dataset: Artificial_unsupervised

https://www.openml.org/d/40892

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Artificial test data set with 4 normal distributions (one of which with low density), a micro cluster and local anomalies. This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40892) of an [OpenML dataset](https://www.openml.org/d/40892). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40892/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40892/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40892/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

